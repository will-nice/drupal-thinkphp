-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1:3306
-- 生成日期： 2021-10-10 12:23:55
-- 服务器版本： 5.7.18-log
-- PHP 版本： 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+08:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `ykccc`
--

-- --------------------------------------------------------

--
-- 表的结构 `ykccc_logs`
--

CREATE TABLE `ykccc_logs` (
  `log_id` bigint(20) NOT NULL COMMENT '日志ID',
  `group_id` varchar(100) DEFAULT NULL COMMENT '服务器组标识',
  `server_id` varchar(100) DEFAULT NULL COMMENT '服务器标识',
  `time` int(11) DEFAULT NULL COMMENT '日志发生时间',
  `level` varchar(15) DEFAULT NULL COMMENT '日志紧急级别',
  `log` text COMMENT '日志消息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='日志表';

--
-- 转储表的索引
--

--
-- 表的索引 `ykccc_logs`
--
ALTER TABLE `ykccc_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `server` (`group_id`,`server_id`),
  ADD KEY `time` (`time`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `ykccc_logs`
--
ALTER TABLE `ykccc_logs`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志ID', AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
