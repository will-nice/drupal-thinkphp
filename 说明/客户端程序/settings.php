<?php
/**
 * Drupal群控客户端V1.0
 * 作者：云客
 * site:www.indrupal.com
 * 微信：indrupal
 * 邮件:phpworld@qq.com
 * 公司：未来很美（深圳）科技有限公司
 * site:www.will-nice.com
 */

/**
 * Drupal群集控制中心（Cluster Control Center），后简称群控中心
 * 是一个运行在远程服务器上的Drupal服务器群集控制系统。
 * 注意它不同于类似阿里云的弹性伸缩（ess）这样的系统，并不提供服务器实例增、减、换等
 * 其主要目的是为群集服务器中的Drupal系统提供配置以控制行为，比如数据库的选择等
 * 有如下功能：
 * 1、向单机版Drupal服务器下发系统配置文件，即通常的sites/default/settings.php文件内容
 * 2、监控Drupal服务器更新状态
 * 3、记录Drupal服务器运行日志等
 *
 * 完整的群控功能由服务器端和客户端两部分组成，本文件为客户端程序，由Drupal系统运行触发
 * 不论是变量还是函数，均以“YK_”作为前缀（yunke的缩写）以避免名称污染
 */

use GuzzleHttp\Client;

/**
 * 本文件主要产生以下三个变量：
 * $settings = []; //其内容注入到settings服务 \Drupal::service('settings')->get('key');
 * $config = []; //释放到全局同名变量
 * $databases = []; //被注入数据库连接类
 * 详见：\Drupal\Core\Site\Settings::initialize
 */

/**********************************群控中心配置部分*******************************/

/**
 * 该函数储存配置信息
 *
 * @return array
 */
function YK_config() {
  $YK_CCC = [];//保存群集控制中心（Cluster Control Center）配置和接口参数
  $YK_CCC['host'] = 'http://www.tp.com/index.php/'; //群控中心主机地址
  $YK_CCC['uri']['settings'] = 'api/settings'; //配置获取接口
  $YK_CCC['uri']['logs'] = 'api/logs'; //日志上传接口
  $YK_CCC['password'] = 'Yunke@20210915';  //通讯签名密钥
  $YK_CCC['secret'] = 'Yunke@20210915'; //机密内容加密密钥
  $YK_CCC['settings'] = __DIR__ . '/YK_settings.php'; //本地配置文件 即原来的settings.php文件
  $YK_CCC['lock'] = __DIR__ . '/YK_lock.php'; //本地配置文件 即原来的settings.php文件
  $YK_CCC['ttl'] = 60; //本地配置文件生成时间 Time To Live 默认值60
  $YK_CCC['group_id'] = 'yunke'; //群集id，用于识别是哪一个群集，一个群集表示提供具体某应用的一组服务器，比如会员系统
  $YK_CCC['server_id'] = 'Server_1'; //服务器id，用于识别具体的服务器 值为在群集中本服务器的标识 比如IP 不同场景可采用不同值
  return $YK_CCC;
}

/**********************************群控中心专用函数*******************************/

/**
 * 使用AES加密算法加密文本
 *
 * @param $plainText String 明文 不限制位数 即便是空字符""都行
 * @param $key       String 对称密钥 不限制位数 即便是空字符""都行，但建议在16个字符
 *
 * @return string base64转码后的密文
 */
function YK_aesEncrypt($plainText, $key) {
  $iv = substr(hash('sha256', $key), 0, 16); //产生初始化加密向量
  return openssl_encrypt($plainText, 'aes-128-cbc', $key, 0, $iv); //也可用aes-128-ctr
}

/**
 * AES解密
 *
 * @param $cipherText String 密文
 * @param $key        String 对称密钥
 *
 * @return string
 */
function YK_aesDecrypt($cipherText, $key) {
  $iv = substr(hash('sha256', $key), 0, 16);
  return openssl_decrypt($cipherText, 'aes-128-cbc', $key, 0, $iv); //也可用aes-128-ctr
}

/**
 * 使用签名密钥进行消息签名
 *
 * @param array $data
 *
 * @return array
 */
function YK_sign($data = []) {
  $YK_CCC = YK_config();
  unset($data['sign']);
  ksort($data);
  $data['sign'] = hash('sha256', YK_aesEncrypt(json_encode($data), $YK_CCC['password']));
  return $data;
}

/**
 * 验证消息签名
 *
 * @param array $data
 *
 * @return bool
 */
function YK_verifySign($data = []) {
  if (empty($data['sign'])) {
    return FALSE;
  }
  $YK_CCC = YK_config();
  $sign = $data['sign'];
  unset($data['sign']);
  ksort($data);
  $calculatedSign = hash('sha256', YK_aesEncrypt(json_encode($data), $YK_CCC['password']));
  if ($sign === $calculatedSign) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * @param null|string $log   日志信息
 * @param string      $level 严重级别：Debug/Info/Warning/Error
 *
 * @return array|void
 */
function YK_logger($log = NULL, $level = 'Info') {
  static $logs = [];
  if ($log === NULL) {
    return $logs;
  }
  if (!in_array($level, ['Debug', 'Info', 'Warning', 'Error'])) {
    $level = 'Info';
  }
  $log = (string) $log;
  if (!isset($logs[$level])) {
    $logs[$level] = [];
  }
  $logs[$level][] = $log;
}

/**
 * 进行日志上传
 *
 * @param array $logs 一个二维数组，第一级键名表示优先级，有如下：Debug/Info/Warning/Error，其值是该优先级日志信息构成的索引数组
 */
function YK_log() {
  $logs = YK_logger();
  if (empty($logs)) {
    return;
  }
  $YK_CCC = YK_config();
  $data = [
    'noise'     => md5(uniqid(microtime(TRUE), TRUE)),
    'time'      => time(),
    'server_id' => $YK_CCC['server_id'],
    'group_id'  => $YK_CCC['group_id'],
    'logs'      => json_encode($logs),
  ];
  $data = YK_sign($data);//添加签名
  $client = new Client();
  $HTTPOptions = [
    'json'    => $data,
    'headers' => ['Accept' => 'application/json'],
  ];
  try {
    $response = $client->post($YK_CCC['host'] . $YK_CCC['uri']['logs'], $HTTPOptions);
    //$responseData = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
    //不需要任何处理
  } catch (\Exception $e) {
    //不需要任何处理
  }
}

/**
 * 群控系统客户端主入口程序
 */
function YK_main() {
  $YK_CCC = YK_config();
  $YK_need_refresh_settings = FALSE;//是否需要刷新本地配置文件
  if (!file_exists($YK_CCC['settings'])) {
    //配置文件不存在则新建
    $YK_need_refresh_settings = TRUE;
    @file_put_contents($YK_CCC['settings'], '<?php');
  }
  else {
    //配置文件存在 则判断是否超过生存期
    $YK_settings_time = (int) filemtime($YK_CCC['settings']);
    if (abs(time() - $YK_settings_time) > $YK_CCC['ttl']) {
      $YK_need_refresh_settings = TRUE;
    }
  }
  if ($YK_need_refresh_settings) {
    //取得锁操作
    $handle = @fopen($YK_CCC['lock'], "r+");
    if ($handle && flock($handle, LOCK_EX | LOCK_NB)) {
      //取得锁操作后执行配置更新
      YK_updateSettings();
      flock($handle, LOCK_UN);
    }
    fclose($handle);
  }
}

/**
 * 实际执行配置更新
 *
 * @return bool
 */
function YK_updateSettings() {
  $YK_CCC = YK_config();
  $data = [
    'noise'     => md5(uniqid(microtime(TRUE), TRUE)),
    'time'      => time(),
    'server_id' => $YK_CCC['server_id'],
    'group_id'  => $YK_CCC['group_id'],
  ];
  $data = YK_sign($data);//添加签名
  $client = new Client();
  $HTTPOptions = [
    'json'    => $data,
    'headers' => ['Accept' => 'application/json'],
  ];
  $responseData = [];
  try {
    $response = $client->post($YK_CCC['host'] . $YK_CCC['uri']['settings'], $HTTPOptions);
    $responseData = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
    //如果返回值不能被解码，此处将为NULL，进而导致验签失败，这多发生在页面出现php错误提示时
    if (YK_verifySign($responseData) == FALSE) {
      YK_logger('消息签名验证失败，拒绝更新配置', 'Warning');
      return FALSE;
    }
    if (isset($responseData['settings']) && !empty($responseData['settings'])) {
      $responseData['settings'] = YK_aesDecrypt($responseData['settings'], $YK_CCC['secret']);
      if (empty($responseData['settings'])) {
        YK_logger('配置信息解密失败，请检查加密密钥', 'Error');
        return FALSE;
      }
      @chmod($YK_CCC['settings'], 0777);
      if (file_put_contents($YK_CCC['settings'], $responseData['settings'])) {
        YK_logger('配置更新成功');
        return TRUE;
      }
      else {
        YK_logger('配置更新写入失败', 'Error');
        return FALSE;
      }
    }
    else {
      YK_logger('请求配置成功，但无配置信息', 'Warning');
      return FALSE;
    }
  } catch (\Exception $e) {
    //HTTP状态码大于等于400 或者网络错误将报错触发这里
    $msg = $e->getMessage() . "\n";
    if ($e->hasResponse()) {
      $msg .= "请求配置失败，响应状态码：" . $e->getResponse()->getStatusCode();
      $msg .= "响应body:" . $e->getResponse()->getBody() . "\n";
    }
    YK_logger($msg, 'Warning');
    return FALSE;
  }
}


/***************************逻辑部分*********************************/
register_shutdown_function('YK_log');
YK_main();
clearstatcache(TRUE);
include YK_config()['settings'];

