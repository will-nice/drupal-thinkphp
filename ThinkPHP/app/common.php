<?php
// 应用公共文件


/**********************************群控中心专用函数*******************************/

/**
 * 使用AES加密算法加密文本
 *
 * @param $plainText String 明文 不限制位数 即便是空字符""都行
 * @param $key       String 对称密钥 不限制位数 即便是空字符""都行，但建议在16个字符
 *
 * @return string base64转码后的密文
 */
function YK_aesEncrypt($plainText, $key) {
  $iv = substr(hash('sha256', $key), 0, 16); //产生初始化加密向量
  return openssl_encrypt($plainText, 'aes-128-cbc', $key, 0, $iv); //也可用aes-128-ctr
}

/**
 * AES解密
 *
 * @param $cipherText String 密文
 * @param $key        String 对称密钥
 *
 * @return string
 */
function YK_aesDecrypt($cipherText, $key) {
  $iv = substr(hash('sha256', $key), 0, 16);
  return openssl_decrypt($cipherText, 'aes-128-cbc', $key, 0, $iv); //也可用aes-128-ctr
}

/**
 * 使用签名密钥进行消息签名
 *
 * @param array $data
 *
 * @return array
 */
function YK_sign($data = []) {
  unset($data['sign']);
  ksort($data);
  $data['sign'] = hash('sha256', YK_aesEncrypt(json_encode($data), config('ykccc.password')));
  return $data;
}

/**
 * 验证消息签名
 *
 * @param array $data
 *
 * @return bool
 */
function YK_verifySign($data = []) {
  if (empty($data['sign'])) {
    return FALSE;
  }
  $sign = $data['sign'];
  unset($data['sign']);
  ksort($data);
  $calculatedSign = hash('sha256', YK_aesEncrypt(json_encode($data), config('ykccc.password')));
  if ($sign === $calculatedSign) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}