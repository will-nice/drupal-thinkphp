<?php
namespace app\model;

use think\Model;

class Logs extends Model
{
  protected $pk = 'log_id';

  /**
   * 时间字段 获取器定义
   * @param $value
   * @param $data
   *
   * @return false|string
   */
  public function getTimeAttr($value,$data)
  {
    return date('Y-m-d H:i:s',$value);
  }
  
}
