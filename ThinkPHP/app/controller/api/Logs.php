<?php

namespace app\controller\api;

use app\BaseController;
use \app\model\Logs as LogsModel;

class Logs extends BaseController {

  /**
   * 保存群集上报的日志信息
   *
   * @return \think\response\Json
   */
  public function index() {
    $data = json_decode($this->request->getInput(), JSON_OBJECT_AS_ARRAY);
    if (empty($data)) {
      return $this->response(['msg' => '请求体无数据', 'code' => 400], 400);
    }
    if (!YK_verifySign($data)) {
      return $this->response(['msg' => '签名验证失败', 'code' => 400], 400);
    }
    $data['logs'] = json_decode($data['logs'], JSON_OBJECT_AS_ARRAY);
    try {
      foreach ($data['logs'] as $level => $logs) {
        foreach ($logs as $log) {
          $logData = [
            'group_id'  => $data['group_id'],
            'server_id' => $data['server_id'],
            'time'      => $data['time'],
            'level'     => $level,
            'log'       => $log,
          ];
          (new LogsModel($logData))->save();
        }
      }
      return $this->response(['msg' => 'ok', 'code' => 200], 200);
    } catch (\Exception $e) {
      return $this->response(['msg' => '日志保存失败', 'code' => 500], 500);
    }

  }

  /**
   * 签名并返回响应数据
   *
   * @param array $responseData json数据数组
   * @param int   $code         状态码
   *
   * @return \think\response\Json
   */
  protected function response($responseData, $code = 200) {
    $responseData = YK_sign($responseData);
    return json($responseData, $code);
  }

}
