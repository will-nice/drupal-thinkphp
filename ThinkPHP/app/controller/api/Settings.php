<?php

namespace app\controller\api;

use app\BaseController;


class Settings extends BaseController {

  /**
   * 为服务器群集下发配置信息
   *
   * @return \think\Response 加密的json响应
   */
  public function index() {
    $data = json_decode($this->request->getInput(), JSON_OBJECT_AS_ARRAY);
    if (empty($data)) {
      return $this->response(['msg' => '请求体无数据', 'code' => 400], 400);
    }
    if (!YK_verifySign($data)) {
      return $this->response(['msg' => '签名验证失败', 'code' => 400], 400);
    }
    $settings = config('ykccc.settings_file');
    if (!is_readable($settings)) {
      return $this->response(['msg' => '无法读取配置信息', 'code' => 500], 500);
    }
    $responseData = [
      'code'     => 200,
      'settings' => @file_get_contents($settings),
    ];
    $responseData['settings'] = YK_aesEncrypt($responseData['settings'], config('ykccc.secret'));
    return $this->response($responseData);
  }

  /**
   * 签名并返回响应数据
   *
   * @param array $responseData json数据数组
   * @param int   $code         状态码
   *
   * @return \think\response\Json
   */
  protected function response($responseData, $code = 200) {
    $responseData = YK_sign($responseData);
    return json($responseData, $code);
  }

}
