<?php
/**
 * API路由定义
 */
use think\facade\Route;



Route::post('api/settings', 'api.Settings/index');
Route::post('api/logs', 'api.Logs/index');
Route::get('test', 'Test/index');
